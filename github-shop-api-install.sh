API_PORT=11230
APP_PATH=/opt/github-shop-api/
SUPERVISOR_CONF=/etc/supervisor/conf.d/github-shop-api.conf

apt-get install supervisor gunicorn

if [ -d $APP_PATH ]; then
    rm $APP_PATH -rf
fi

git clone https://github.com/sikaondrej/github-shop-api $APP_PATH

if [ -f $SUPERVISOR_CONF ]; then
    supervisorctl restart github-shop-api
else
    echo "[program:github-shop-api]
command = gunicorn wsgi:application -b 0.0.0.0:$API_PORT
directory = $APP_PATH
" > $SUPERVISOR_CONF
    
    supervisorctl reload
fi
