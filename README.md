install
=======


Alsa remote
-----------

    wget https://raw.github.com/sikaondrej/install-scripts/master/alsa-remote-install.sh
    sudo sh alsa-remote-install.sh

Github shop API
---------------

    wget https://raw.github.com/sikaondrej/install-scripts/master/github-shop-api-install.sh
    sudo sh alsa-remote-install.sh

TyperJS server
--------------

    wget https://raw.github.com/sikaondrej/install-scripts/master/typerjs-install.sh
    sudo sh typerjs-install.sh

Node JS 0.10.17
---------------

    wget https://raw.github.com/sikaondrej/install-scripts/master/node-install.sh
    sudo sh node-install.sh
