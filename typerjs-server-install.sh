API_PORT=11240
APP_PATH=/opt/typerjs-server/
SUPERVISOR_CONF=/etc/supervisor/conf.d/typerjs-server.conf

apt-get install supervisor gunicorn python-virtualenv

if [ -d $APP_PATH ]; then
    rm $APP_PATH -rf
fi

git clone https://github.com/sikaondrej/typerjs-server $APP_PATH

cd $APP_PATH
sh git-depences.sh
virtualenv env
env/bin/pip install -r requirements.txt


if [ -f $SUPERVISOR_CONF ]; then
    supervisorctl restart typerjs-server
else
    echo "[program:typerjs-server]
command = gunicorn wsgi:application -b 0.0.0.0:$API_PORT
directory = $APP_PATH
" > $SUPERVISOR_CONF
    
    supervisorctl reload
fi
