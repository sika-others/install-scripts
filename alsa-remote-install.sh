APP_PATH=/opt/alsaremote/
SUPERVISOR_CONF=/etc/supervisor/conf.d/alsaremote.conf

apt-get install supervisor gunicorn python-setuptools

if [ -d $APP_PATH ]; then
    rm $APP_PATH -rf
fi

git clone https://github.com/sikaondrej/alsa-remote.git $APP_PATH

if [ -f $SUPERVISOR_CONF ]; then
    supervisorctl restart alsaremote
else
    echo "[program:alsaremote]
command = gunicorn wsgi:application -b 0.0.0.0:11220
directory = $APP_PATH
" > $SUPERVISOR_CONF
    
    supervisorctl reload
fi
